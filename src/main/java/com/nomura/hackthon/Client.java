package com.nomura.hackthon;

public class Client {

    private int no;

    private String businessUnit;

    private int accountNo;

    private String clientInventmentRishProfile;

    private String clientType;

    private String accreditedInvestor;

    private String professionalInvestor;

    private Boolean derivativeKnowledge;

    private Boolean HKNexues;

    private Boolean SGNexues;

    private Boolean bondsVanillaFlag;

    private String bondsVanillaDesc;

    private Boolean imPack2;

    private Boolean imPack4;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public int getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(int accountNo) {
        this.accountNo = accountNo;
    }

    public String getClientInventmentRishProfile() {
        return clientInventmentRishProfile;
    }

    public void setClientInventmentRishProfile(String clientInventmentRishProfile) {
        this.clientInventmentRishProfile = clientInventmentRishProfile;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getAccreditedInvestor() {
        return accreditedInvestor;
    }

    public void setAccreditedInvestor(String accreditedInvestor) {
        this.accreditedInvestor = accreditedInvestor;
    }

    public String getProfessionalInvestor() {
        return professionalInvestor;
    }

    public void setProfessionalInvestor(String professionalInvestor) {
        this.professionalInvestor = professionalInvestor;
    }

    public Boolean getDerivativeKnowledge() {
        return derivativeKnowledge;
    }

    public void setDerivativeKnowledge(Boolean derivativeKnowledge) {
        this.derivativeKnowledge = derivativeKnowledge;
    }

    public Boolean getHKNexues() {
        return HKNexues;
    }

    public void setHKNexues(Boolean HKNexues) {
        this.HKNexues = HKNexues;
    }

    public Boolean getSGNexues() {
        return SGNexues;
    }

    public void setSGNexues(Boolean SGNexues) {
        this.SGNexues = SGNexues;
    }

    public Boolean getBondsVanillaFlag() {
        return bondsVanillaFlag;
    }

    public void setBondsVanillaFlag(Boolean bondsVanillaFlag) {
        this.bondsVanillaFlag = bondsVanillaFlag;
    }

    public String getBondsVanillaDesc() {
        return bondsVanillaDesc;
    }

    public void setBondsVanillaDesc(String bondsVanillaDesc) {
        this.bondsVanillaDesc = bondsVanillaDesc;
    }

    public Boolean getImPack2() {
        return imPack2;
    }

    public void setImPack2(Boolean imPack2) {
        this.imPack2 = imPack2;
    }

    public Boolean getImPack4() {
        return imPack4;
    }

    public void setImPack4(Boolean imPack4) {
        this.imPack4 = imPack4;
    }
}
