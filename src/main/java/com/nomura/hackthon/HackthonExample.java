package com.nomura.hackthon;

import com.nomura.businessrule.ibtis.DaoImpl;
import org.drools.compiler.DroolsParserException;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.drools.RuleBase;
import org.drools.RuleBaseFactory;
import org.drools.WorkingMemory;
import org.drools.compiler.PackageBuilder;
import org.drools.rule.Package;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class HackthonExample {

    public static void main(String[] args) throws Exception {
        //load up the rulebase
        RuleBase ruleBase = readDecisionTable();
        WorkingMemory workingMemory =  ruleBase.newStatefulSession();

        DaoImpl dao = new DaoImpl();
        List<Transaction> transactionList = dao.selectTransactions();

        Transaction transaction = new Transaction();
        transaction.setRequestType("Solicited");
        transaction.setbPPortfolioType("not Discretionary");
        transaction.setAssetCoverage("Bonds");


        Product product = new Product();
        product.setNomuraAssetType("FXSpot");

        workingMemory.insert(product);
        workingMemory.insert(transaction);
        workingMemory.fireAllRules();


        //session.execute(product);
//        for (Transaction transaction : transactionList) {
//            session.execute(transaction);
//        }




//        System.out.println("First Transaction\n" + transaction);

//        Customer newCustomer = Customer.newCustomer();
//        newCustomer.addItem(p1, 1);
//        newCustomer.addItem(p2, 2);
//        session.execute(newCustomer);
//
//        System.out.println("*********************************");
//        System.out.println("Second Customer\n" + customer);
    }

//    private static KnowledgeBase createKnowledgeBaseFromSpreadsheet() throws Exception {
//        DecisionTableConfiguration dtconf = KnowledgeBuilderFactory.newDecisionTableConfiguration();
//        dtconf.setInputType(DecisionTableInputType.XLS);
//        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
//        knowledgeBuilder.add(ResourceFactory.newClassPathResource("rules/IWAHackthonRules1.xlsx"), ResourceType.DTABLE,
//                dtconf);
//        if (knowledgeBuilder.hasErrors()) {
//            throw new RuntimeException(knowledgeBuilder.getErrors().toString());
//        }
//        KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
//        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
//        return knowledgeBase;
//    }

    private static RuleBase readDecisionTable() throws IOException {
        //read in the source
        final SpreadsheetCompiler converter = new SpreadsheetCompiler();
        final String drl = converter.compile("/rules/IWAHackthonRules.xls", InputType.XLS);
        System.out.println(drl);
        PackageBuilder builder = new PackageBuilder();
        try {
            builder.addPackageFromDrl(new StringReader(drl));
        } catch (DroolsParserException e) {
            e.printStackTrace();
        }
        Package pkg = builder.getPackage();

        RuleBase ruleBase = RuleBaseFactory.newRuleBase();
        ruleBase.addPackage( pkg );
        return ruleBase;
    }
}
