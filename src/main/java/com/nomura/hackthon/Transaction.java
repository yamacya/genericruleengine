package com.nomura.hackthon;

import java.util.Date;

public class Transaction {

    private int no;

    private int clientNo;

    private int productNo;

    private Date time;

    private String requestType;

    private String bPPortfolioType;

    private String assetCoverage;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getClientNo() {
        return clientNo;
    }

    public void setClientNo(int clientNo) {
        this.clientNo = clientNo;
    }

    public int getProductNo() {
        return productNo;
    }

    public void setProductNo(int productNo) {
        this.productNo = productNo;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getbPPortfolioType() {
        return bPPortfolioType;
    }

    public void setbPPortfolioType(String bPPortfolioType) {
        this.bPPortfolioType = bPPortfolioType;
    }

    public String getAssetCoverage() {
        return assetCoverage;
    }

    public void setAssetCoverage(String assetCoverage) {
        this.assetCoverage = assetCoverage;
    }

    public void getMessage(String message){
        System.out.println("This is suggested information on the transaction " + getNo() + ": " + message);
    }
}
