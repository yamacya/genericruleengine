package com.nomura.hackthon;

import java.math.BigDecimal;
import java.util.Date;

public class Product {

    private int no;

    private int internalID;

    private String sortAlpha;

    private String ISIN;

    private String nomuraAssetGroup;

    private String nomuraAssetSubGroup;

    private String nomuraAssetType;

    private String bloombergCode;

    private String market;

    private int priority;

    private String tradingCcy;

    private String isTradingPlace;

    private String getPrice;

    private BigDecimal closePrice;

    private Date priceDate;

    private String Blocked;

    private String listingStatus;

    private String DervativeKnowledge;

    private String sfcAuthorized;

    private String masAuthorized;

    private String hkexChapter37;

    private String coverageList;

    private String complexProduct;

    private String liwuidAsset;

    private String prr;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getInternalID() {
        return internalID;
    }

    public void setInternalID(int internalID) {
        this.internalID = internalID;
    }

    public String getSortAlpha() {
        return sortAlpha;
    }

    public void setSortAlpha(String sortAlpha) {
        this.sortAlpha = sortAlpha;
    }

    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    public String getNomuraAssetGroup() {
        return nomuraAssetGroup;
    }

    public void setNomuraAssetGroup(String nomuraAssetGroup) {
        this.nomuraAssetGroup = nomuraAssetGroup;
    }

    public String getNomuraAssetSubGroup() {
        return nomuraAssetSubGroup;
    }

    public void setNomuraAssetSubGroup(String nomuraAssetSubGroup) {
        this.nomuraAssetSubGroup = nomuraAssetSubGroup;
    }

    public String getNomuraAssetType() {
        return nomuraAssetType;
    }

    public void setNomuraAssetType(String nomuraAssetType) {
        this.nomuraAssetType = nomuraAssetType;
    }

    public String getBloombergCode() {
        return bloombergCode;
    }

    public void setBloombergCode(String bloombergCode) {
        this.bloombergCode = bloombergCode;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTradingCcy() {
        return tradingCcy;
    }

    public void setTradingCcy(String tradingCcy) {
        this.tradingCcy = tradingCcy;
    }

    public String getIsTradingPlace() {
        return isTradingPlace;
    }

    public void setIsTradingPlace(String isTradingPlace) {
        this.isTradingPlace = isTradingPlace;
    }

    public String getGetPrice() {
        return getPrice;
    }

    public void setGetPrice(String getPrice) {
        this.getPrice = getPrice;
    }

    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    public Date getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(Date priceDate) {
        this.priceDate = priceDate;
    }

    public String getBlocked() {
        return Blocked;
    }

    public void setBlocked(String blocked) {
        Blocked = blocked;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public String getDervativeKnowledge() {
        return DervativeKnowledge;
    }

    public void setDervativeKnowledge(String dervativeKnowledge) {
        DervativeKnowledge = dervativeKnowledge;
    }

    public String getSfcAuthorized() {
        return sfcAuthorized;
    }

    public void setSfcAuthorized(String sfcAuthorized) {
        this.sfcAuthorized = sfcAuthorized;
    }

    public String getMasAuthorized() {
        return masAuthorized;
    }

    public void setMasAuthorized(String masAuthorized) {
        this.masAuthorized = masAuthorized;
    }

    public String getHkexChapter37() {
        return hkexChapter37;
    }

    public void setHkexChapter37(String hkexChapter37) {
        this.hkexChapter37 = hkexChapter37;
    }

    public String getCoverageList() {
        return coverageList;
    }

    public void setCoverageList(String coverageList) {
        this.coverageList = coverageList;
    }

    public String getComplexProduct() {
        return complexProduct;
    }

    public void setComplexProduct(String complexProduct) {
        this.complexProduct = complexProduct;
    }

    public String getLiwuidAsset() {
        return liwuidAsset;
    }

    public void setLiwuidAsset(String liwuidAsset) {
        this.liwuidAsset = liwuidAsset;
    }

    public String getPrr() {
        return prr;
    }

    public void setPrr(String prr) {
        this.prr = prr;
    }
}
