package com.nomura.businessrule.ibtis;

import com.nomura.businessrule.Customer;

public interface CustomerMapper {

    Customer selectCustomer(int id);
}
