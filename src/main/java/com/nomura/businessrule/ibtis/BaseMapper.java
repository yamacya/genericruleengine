package com.nomura.businessrule.ibtis;

import java.util.List;

public interface BaseMapper<T> {

    List<T> selectAll();
}
