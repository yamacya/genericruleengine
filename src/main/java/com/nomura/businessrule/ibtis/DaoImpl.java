package com.nomura.businessrule.ibtis;

import com.nomura.businessrule.Country;
import com.nomura.hackthon.Transaction;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class DaoImpl {

    private static SqlSessionFactory sqlSessionFactory;

    static {
        String config = "ibatis/SqlMapConfig.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(config);
            sqlSessionFactory =
                    new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Country selectCountry(int id) {
        List<Country> country;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            BaseMapper mapper = session.getMapper(BaseMapper.class);
            country = mapper.selectAll();
        }
        System.out.println("Records Read Successfully ");
        return country.get(0);
    }

    public List<Transaction> selectTransactions() {
        List<Transaction> transactions;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            BaseMapper mapper = session.getMapper(BaseMapper.class);
            transactions = mapper.selectAll();
        }
        return transactions;
    }

    public static void main(String[] args) {
        DaoImpl dao = new DaoImpl();
        Country country = dao.selectCountry(1);
        System.out.println(country.getValue());
    }
}
