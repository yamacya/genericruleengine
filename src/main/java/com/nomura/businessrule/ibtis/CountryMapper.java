package com.nomura.businessrule.ibtis;

import com.nomura.businessrule.Country;

public interface CountryMapper {

    Country selectCountry(int id);
}
