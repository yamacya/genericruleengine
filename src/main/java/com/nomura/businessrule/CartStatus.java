package com.nomura.businessrule;

public enum CartStatus {
NEW,
PROCESSED,
PENDING
}
